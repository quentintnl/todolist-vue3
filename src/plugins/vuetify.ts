/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  theme: {
    themes: {
      light: {
        colors: {
          primary: '#1867C0',
          secondary: '#5CBBF6',
          purple: '#B39DDB',
          blueLighten: '#BBDEFB',
          pinkLighten: '#F8BBD0',
          greenLight: '#C5E1A5',
          yellow: '#FFF9C4',
          grey: '#616161',
          greyLighten: '#E0E0E0'
        },
      },
    },
  },
})

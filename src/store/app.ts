// Utilities
import { defineStore } from 'pinia'
import TodoJson from '@/utils/todo.json'
import { ref, computed } from 'vue'

export const useAppStore = defineStore('app', () => {
  const todoJson = ref(TodoJson)
  const todoJsonSort = ref({})

  const currentTodoJson = computed(() => {
    return Object.keys(todoJsonSort.value).length > 0 ? todoJsonSort.value : todoJson.value
  })

  const updateTodoJsonSort = (newTodoJsonSort: any) => {
    todoJsonSort.value = newTodoJsonSort
  }

  return {
    todoJson,
    todoJsonSort,
    currentTodoJson,
    updateTodoJsonSort
  }
})
